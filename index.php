<?php
//Inicio la session
session_start();

?>
<?php

// Pregunto si el boton 'btnAgregar' fue clickeado y si es asi, guardo en variables locales todos los parametros del formulario correspondiente al boton
if (isset($_POST['btnAgregar'])) {
    $id = $_POST["id"];
    $producto = $_POST["producto"];
    $cantidad = $_POST["cantidad"];
    $precio = $_POST["precio"];

    // Pregunto si el carrito esta vacio. 
    if (!isset($_SESSION['carrito'])) {
        // Si el carrito esta vacio guardo las variables en un array de session con un indice '0'
        $carrito_actual = ['id' => $id, 'producto' => $producto, 'cantidad' => $cantidad, 'precio' => $precio];
        $_SESSION['carrito'][0] = $carrito_actual;
    } else {
        $num_agregado = 0;
        //Si el carrito ya tiene un producto cargado, primero cuento cuantos productos tiene
        $num_agregado = count($_SESSION['carrito']);
        // Guardo las variables del producto en un array de session en la posicion de  'count'
        $carrito_actual = ['id' => $id, 'producto' => $producto, 'cantidad' => $cantidad, 'precio' => $precio];
        $_SESSION['carrito'][$num_agregado] = $carrito_actual;
    }
}

include 'templates/cabecera.php';

?>

<!-- Formulario interactivo con el usuario -->
<div class="row justify-content-center " style="margin: 80px ;">

    <div class="card" style="width: 20rem; margin: 15px;">
        <img max-width= 300px; max-height= 300px src="https://http2.mlstatic.com/D_NQ_NP_2X_812986-MLA48050585985_102021-F.webp" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Guitarra Cort "A"</h5>
            <p class="card-text">Guitarra de estudio</p>
            <p>
            <h3>$500000</h3>
            </p>
            <form action="index.php" method="post">
                <input type="hidden" name="id" value=1>
                <input type="hidden" name="producto" value="Cort A">
                <input type="hidden" name="precio" value=500000>
                <input type="number" placeholder="Unidades" name="cantidad" style="width: 87px">
                <input type="submit" value="Agregar al carrito" name="btnAgregar" class="btn btn-success">
            </form>
        </div>
    </div>


    <div class="card" style="width: 20rem; margin: 15px;">
        <img src="https://http2.mlstatic.com/D_NQ_NP_2X_800189-MLA48802518692_012022-F.webp" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Guitarra Cort "B"</h5>
            <p class="card-text">Guitarra profesional</p>
            <p>
            <h3>$150000</h3>
            </p>
            <form action="index.php" method="post">
                <input type="hidden" name="id" value=2>
                <input type="hidden" name="producto" value="Cort B">
                <input type="hidden" name="precio" value=150000>
                <input type="number" placeholder="Unidades" name="cantidad" style="width: 87px">
                <input type="submit" value="Agregar al carrito" name="btnAgregar" class="btn btn-success">
            </form>
        </div>
    </div>


    <div class="card" style="width: 20rem; margin: 15px;">
        <img src="https://http2.mlstatic.com/D_NQ_NP_2X_752039-MLA48484800499_122021-F.webp class" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Guitarra Cort "C"</h5>
            <p class="card-text">Guitarra semi-profesional</p>
            <p>
            <h3>$300000</h3>
            </p>
            <form action="index.php" method="post">
                <input type="hidden" name="id" value=3>
                <input type="hidden" name="producto" value="Cort C">
                <input type="hidden" name="precio" value=300000>
                <input type="number" placeholder="Unidades" name="cantidad" style="width: 87px">
                <input type="submit" value="Agregar al carrito" name="btnAgregar" class="btn btn-success">
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<?php
include 'templates/pie.php';
?>